###neural progenitor cells (NPCs)
VlnPlot(fc.s.combined,features = c("PAX6","EOMES","GFAP","NES","SOX2","PROX1","GFAP","SLC1A3","FABP7","ASCL1","DLX2","DCX","NCAM1"),pt.size = 0,ncol = 3,assay = "RNA",slot = "counts", log = TRUE)
VlnPlot(fc.s.combined,features = c("CUX2","BCL11B","FEZF2","NTF3"),pt.size = 0,ncol = 3,assay = "RNA",slot = "counts", log = TRUE)
for(i in 0:max(as.numeric(Sample_merge@ident))-1)
{
  gene<-intersect(TF$V1,markers[which(markers$cluster==i),7])
  for(j in gene)
  {
    exp<-as.matrix(Sample_merge@data[j,])
    clu<-as.matrix(Sample_merge@ident)
    col.num<-max(as.numeric(unique(clu[,1])))
    cell<-rownames(as.matrix(Sample_merge@ident))
    mydata<-data.frame(cell,clu,exp)
    mydata<-as.data.frame(mydata)
    mydata$clu=factor(mydata$clu,levels =c(0:max(as.numeric(Sample_merge@ident))-1))
    #View(mydata)
    p<-(ggplot(mydata,aes(x=clu,y=exp,fill=clu))+geom_boxplot(outlier.colour = NA))+
      scale_fill_manual(values=colorRampPalette(brewer.pal(9,"Set3"))(col.num+1))+ggtitle(j)+
      theme(plot.background = element_rect(colour = NA),panel.grid.major = element_line(colour = NA),
            panel.grid.minor = element_line(colour = NA),
            panel.border = element_rect(colour = NA),axis.line = element_line(colour = "black"))
    pdf(file=paste("boxplot_rec_","clu",i,"_",j,".pdf",sep=""),height = 3,width = 9)
    print(p)
    dev.off()
  }
}
FeaturePlot(object = fc.s.combined, features = c("PAX6","EOMES"), cols = c("lightgrey", "blue"),pt.size = 0.1,min.cutoff = 0)


##EX
VlnPlot(fc.s.combined,features = c("RBFOX3","SLC17A7","SATB2","GLRA3","CUX2","BHLHE22","RORB","FOXP2","FEZF2","HTR2C","OPRK1","NR4A2"),pt.size = 0,ncol = 3,assay = "RNA",slot = "counts", log = TRUE)
VlnPlot(fc.s.combined,features = c("NEUROD2","CPLX2","CLU","SH3GLB2","CST3","MT2A","NTRK2","NRGN"),pt.size = 0,ncol = 3,assay = "RNA",slot = "counts", log = TRUE)
VlnPlot(fc.s.combined,features = c("NEUROD2","ZEB1","REST"),pt.size = 0,ncol = 3,assay = "RNA",slot = "counts", log = TRUE)


FeaturePlot(object = fc.s.combined, features = c("SATB2"), cols = c("lightgrey", "blue"),pt.size = 0.1,min.cutoff = 0)

##inhibitory neuron			
VlnPlot(fc.s.combined,features = c("NXPH1","SLC6A1","SLC32A1","GAD1","GAD2","SST","VIP","RELN","PVALB","PDE9A","NDNF","SV2C","SULF1","NOS1"),ncol = 3,pt.size = 0,assay = "RNA",slot = "counts", log = TRUE)
VlnPlot(fc.s.combined,features = c("GABRA2","BCYRN1","ASNS","NRGN","MGLL","TRAP1","NLGN2"),ncol = 3,pt.size = 0,assay = "RNA",slot = "counts", log = TRUE)
VlnPlot(fc.s.combined,features = c("ERBB4","CXCR4","DLX1","DLX2","DLX6-AS1","GAD1"),ncol = 3,pt.size = 0,assay = "RNA",slot = "counts", log = TRUE)

##microglia
VlnPlot(fc.s.combined,features = c("PTPRC", "P2RY12"),assay = "RNA",log = T,pt.size = 0)

##astrocytes
VlnPlot(fc.s.combined,features = c("AQP4","GFAP"),pt.size = 0,ncol = 2,assay = "RNA",slot = "counts", log = TRUE)

##oligodendrocyte progenitor cells (OPCs)		
VlnPlot(fc.s.combined,features = c("OLIG1","COL20A1", "PMP2","OLIG2","PDGRFA"),pt.size = 0,ncol = 2,assay = "RNA",slot = "counts", log = TRUE)
FeaturePlot(object = fc.s.combined, features = c("OLIG1","COL20A1", "PMP2","OLIG2"), cols = c("lightgrey", "blue"),pt.size = 0.1,min.cutoff = 0)

##endothelial cells
VlnPlot(fc.s.combined,features = c("ALAS2", "HBG1","CD74","HBB","B2M","IGFBP7", "COL4A2", "HES1"),ncol = 3,assay = "RNA",log = T,pt.size = 0)