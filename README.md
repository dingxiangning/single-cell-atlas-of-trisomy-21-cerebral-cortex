# Single cell atlas of trisomy 21 cerebral cortex

In this project, we profiled the transcriptome of 36,046 cells in cerebral cortex of trisomy 21 human fetus, covering frontal lobe, parietal lobe, occipital lobe and temporal lobe. 

R script of motif scanning analysis.