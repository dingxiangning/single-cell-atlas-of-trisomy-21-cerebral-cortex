setwd("./../../TC/hcluster/")
dir.create("endo")
setwd("endo")
endo<-c("ALAS2", "HBG1","CD74","HBB","B2M","IGFBP7", "COL4A2", "HES1")
plotMarker(tm.s.combined,endo)

setwd("./../")
dir.create("OPC")
setwd("OPC/")
#"OLIG1","COL20A1", "PMP2","OLIG2","PDGRFA"
OPC<-c("PDGFRA","CSPG4","OLIG1","COL20A1","PMP2","OLIG2","PDGRFA")
OPC<-intersect(OPC,rownames(fc.s.combined@assays$RNA))
plotMarker(tm.s.combined,OPC)

setwd("./../excitatory neurotransmitters/")
EX<-c("RBFOX3","SLC17A7","SATB2","GLRA3","CUX2","BHLHE22","RORB","FOXP2","FEZF2","HTR2C","OPRK1","NR4A2","NEUROD2","CPLX2","CLU","SH3GLB2","CST3","MT2A","NTRK2","NRGN","NEUROD2","ZEB1","REST")
plotMarker(tm.s.combined,EX)

setwd("./../inhibitory neurotransmitters/")
IN<-c("NXPH1","SLC6A1","SLC32A1","GAD1","GAD2","SST","VIP","RELN","PVALB","PDE9A","NDNF","SV2C","SULF1","NOS1","GABRA2","BCYRN1","ASNS","NRGN","MGLL","TRAP1","NLGN2","ERBB4","CXCR4","DLX1","DLX2","DLX6-AS1","GAD1")
plotMarker(tm.s.combined,IN)

setwd("./../")
dir.create("NPC")
setwd("NPC")
NPC<-c("PAX6","EOMES","GFAP","NES","SOX2","PROX1","GFAP","SLC1A3","FABP7","ASCL1","DLX2","DCX","NCAM1","CUX2","BCL11B","FEZF2","NTF3")
plotMarker(tm.s.combined,NPC)

setwd("astrocytes")
astrocytes<-c("GFAP","GLAST","SLC1A2","GLUL","S100B","ALDH1L1","AQP4")
astrocytes<-intersect(astrocytes,rownames(fc.s.combined@assays$RNA))
plotMarker(tm.s.combined,astrocytes)

tm.markers <- FindAllMarkers(tm.s.combined, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
fc.markers <- FindAllMarkers(fc.s.combined, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)

##function: Go & KEGG analysis
library(clusterProfiler)
GetGo <- function(list ,name = "genes"){
  library(clusterProfiler)
  library(ggplot2)
  library(org.Hs.eg.db)
  for (cluster in unique(list[,"cluster"])) {
    gene <- as.character(list[which(list$cluster==cluster),7])
    mygene<-select(org.Hs.eg.db,columns=c("SYMBOL","ENTREZID"),keytype="SYMBOL",keys=gene)
    mygene<-mygene$ENTREZID
    ego<-enrichGO(OrgDb = "org.Hs.eg.db",gene=mygene,ont="BP",pvalueCutoff=0.05,readable=TRUE)
    write.table(ego,paste("GO_",name,"_",cluster,".txt",sep=""),sep="\t",row.names = F,quote = F)
    pathway<-as.data.frame(ego)
    pathway = pathway[1:20,]
    pathway = pathway[order(pathway$pvalue,decreasing=T),]
    pathway <- na.omit(pathway)
    tt <- factor(pathway$Description, levels=unique(pathway$Description))
    pp = ggplot(pathway,aes(-1*log10(p.adjust),tt))
    pbubble = pp +geom_point(aes(size=Count,color=-1*log10(p.adjust)))+
      scale_colour_gradient(low="blue",high="red") +
      theme_bw() +scale_size(range=c(2, 10)) +
      labs(title = paste("GO_",name,sep=""))+
      ylab("")+xlim(min(-1*log10(pathway$p.adjust))*0.9, max(-1*log10(pathway$p.adjust))*1.1)+
      theme(axis.text.y = element_text(size = 20, family = "Helvetica", color = "black",  angle = 0),
            axis.text.x = element_text(size = 15, family = "Helvetica", color = "black", angle = 0))+
      theme(legend.title = element_text(color="black", size=20, family = "Helvetica"))+
      theme(legend.text = element_text(color="azure4", size = 15,  family = "Helvetica"))+
      theme(axis.title = element_text(color="black", size=16, family = "Helvetica"))+
      theme(legend.key.size=unit(1.1,'cm'))
    ggsave(file=paste("GO_",name,"_",cluster,".pdf",sep=""), plot=pbubble, width=20, height=10)
    
    # KEGG enrichment analysis  
    ekk<-enrichKEGG(gene=mygene,organism = "hsa",pvalueCutoff=0.05)
    write.table(ekk,paste("KEGG_",name,"_",cluster,".txt",sep=""),sep="\t",row.names = F,quote = F)
    pathway<-as.data.frame(ekk)
    pathway = pathway[1:20,]
    pathway = pathway[order(pathway$pvalue,decreasing=T),]
    pathway <- na.omit(pathway)
    tt <- factor(pathway$Description, levels=unique(pathway$Description))
    pp = ggplot(pathway,aes(-1*log10(p.adjust),tt))
    pbubble = pp +geom_point(aes(size=Count,color=-1*log10(p.adjust)))+
      scale_colour_gradient(low="blue",high="red") +theme_bw() +
      scale_size(range=c(2, 10)) +
      labs(title = paste("KEGG_",name,sep=""))+ylab("")+
      theme(axis.text.y = element_text(size = 20, family = "Helvetica", color = "black", angle = 0),
            axis.text.x = element_text(size = 15, family = "Helvetica", color = "black",  angle = 0))+
      theme(legend.title = element_text(color="black", size=20, family = "Helvetica"))+
      theme(legend.text = element_text(color="azure4", size = 15,  family = "Helvetica"))+
      theme(axis.title = element_text(color="black", size=16, family = "Helvetica"))+
      theme(legend.key.size=unit(1.1,'cm'))
    ggsave(file=paste("KEGG_",name,"_",cluster,".pdf",sep=""), plot=pbubble, width=15, height=9)
  }
}
dir.create("go_kegg")
setwd("go_kegg")
GetGo(tm.markers)

#general marker nuclear
FeaturePlot(tm.s.combined,"NEUROD1")
FeaturePlot(fc.s.combined,"NEUROD1")

#serotonergic neuron “FEV”,"DCC"
FeaturePlot(tm.s.combined,c("SLC18A2"))
FeaturePlot(fc.s.combined,c("DCC"))
plotMarker(fc.s.combined,"DCC")

#SLC32A1
#clycinergic
FeaturePlot(tm.s.combined,c("SLC32A1"))
FeaturePlot(fc.s.combined,c("SLC32A1"))
plotMarker(fc.s.combined,"SLC32A1")

##steady-state microglia
#AIF1
FeaturePlot(fc.s.combined,c("AIF1"))

#M1 mic NOS2 PTGS2
FeaturePlot(tm.s.combined,c("PPARG"))
FeaturePlot(fc.s.combined,c("PTGS2"))

#AST LGALS3 NKX2-2
FeaturePlot(tm.s.combined,c("LGALS3"))
FeaturePlot(tm.s.combined,c("SOX9"))
FeaturePlot(tm.s.combined,c("HES1"))
FeaturePlot(tm.s.combined,c("NOTCH1"))
FeaturePlot(tm.s.combined,c("ALDH1L1"))
FeaturePlot(tm.s.combined,c("GJA1"))
FeaturePlot(tm.s.combined,c("SLC1A3"))
FeaturePlot(tm.s.combined,c("GLT1"))

FeaturePlot(fc.s.combined,c("LGALS3"))
FeaturePlot(fc.s.combined,c("SOX9"))
FeaturePlot(fc.s.combined,c("HES1"))
FeaturePlot(fc.s.combined,c("NOTCH1"))
FeaturePlot(fc.s.combined,c("ALDH1L1"))
FeaturePlot(fc.s.combined,c("GJA1"))
FeaturePlot(fc.s.combined,c("SLC1A3"))
FeaturePlot(fc.s.combined,c("NKX2-2"))

#OPC
FeaturePlot(tm.s.combined,c("NKX2-2"))

#immature OLG 
FeaturePlot(tm.s.combined,c("CNP"))
FeaturePlot(fc.s.combined,c("CNP"))

#mature olig RTN4
FeaturePlot(tm.s.combined,c("APC"))
FeaturePlot(tm.s.combined,c("RTN4"))
FeaturePlot(tm.s.combined,c("PLP1"))
FeaturePlot(fc.s.combined,c("APC"))
FeaturePlot(fc.s.combined,c("RTN4"))
FeaturePlot(fc.s.combined,c("PLP1"))






