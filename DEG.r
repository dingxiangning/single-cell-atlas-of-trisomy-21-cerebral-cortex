#data <- read.table(unz("fc singlet combined2.Rdata.zip", "fc.s.combined"))
unzip("fc singlet combined2.Rdata.zip")
load("fc singlet combined2.Rdata")

load("trajectory191114/fc_tra_all_data.RData")

DEX<-read.csv("DEXofDFC.csv")
DEX<-DEX[,c(3,5,6,7)]

#filter
DEX<-DEX[which(DEX$fold_difference..log2. > 0.5 | DEX$fold_difference..log2.< (-0.5)),]

#pathway
fgseaRes <- fgsea(pathways = examplePathways, 
                  stats = exampleRanks,
                  minSize=15,
                  maxSize=500,
                  nperm=100000)

##marker
mydata$clu<-factor(mydata$clu,levels = c("17","9_A","15","9_B","13","18","19","1","0","16","7","8","4","6","11","14","5","10","12","2","3"))
mydata$V4<-factor(rep("EX",dim(mydata)[1]),levels = c("AST","OPC","NPC","MIC","Glia","EX","IN"))
mydata[which(mydata$clu %in% c("0","1","2","3","5","10","11","12","14","16")),4]<-c(rep("EX",length(mydata[which(mydata$clu %in% c("0","1","2","3","5","10","11","12","14","16")),4])))
mydata[which(mydata$clu %in% c("4","6","7","8")),4]<-c(rep("IN",length(mydata[which(mydata$clu %in% c("4","6","7","8")),4])))
mydata[which(mydata$clu %in% c("18","19")),4]<-c(rep("Glia",length(mydata[which(mydata$clu %in% c("18","19")),4])))
mydata[which(mydata$clu %in% c("9_B","13","15")),4]<-c(rep("NPC",length(mydata[which(mydata$clu %in% c("9_B","13","15")),4])))
mydata[which(mydata$clu %in% c("9_A")),4]<-c(rep("OPC",length(mydata[which(mydata$clu %in% c("9_A")),4])))
mydata[which(mydata$clu %in% c("17")),4]<-c(rep("AST",length(mydata[which(mydata$clu %in% c("17")),4])))

#mydata$V4<-factor(rep("EX",dim(mydata)[1]),levels = c("EX","IN","AST","OPC","NPC","MIC","Glia"))

fc.markers_f<-cbind(fc.markers,ct=factor(rep("EX",dim(fc.markers)[1]),levels = c("AST","OPC","NPC","MG","EN","EX","IN")))

fc.markers_f[which(fc.markers_f$cluster %in% c("0","1","2","3","5","10","11","12","14","16")),8]<-"EX"
fc.markers_f[which(fc.markers_f$cluster %in% c("4","6","7","8")),8]<-"IN"

fc.markers_f[which(fc.markers_f$cluster %in% c("17")),8]<-"AST"
fc.markers_f[which(fc.markers_f$cluster %in% c("9_A")),8]<-"OPC"
fc.markers_f[which(fc.markers_f$cluster %in% c("13","15","9_B")),8]<-"NPC"
fc.markers_f[which(fc.markers_f$cluster %in% c("18")),8]<-"MG"
fc.markers_f[which(fc.markers_f$cluster %in% c("19")),8]<-"EN"

fc.markers_all <- FindAllMarkers(fc.s.combined, only.pos = FALSE, min.pct = 0.25, logfc.threshold = 0.25)


epilepsy_marker_plot<-fc.markers_all[which(fc.markers_all$gene %in% e_arr),c(2,6,7)]
library(pheatmap)
library(reshape2)
melt(epilepsy_marker_plot,id=c())

library(edgeR)

library(tidyr)
heatmap_ep<-tidyr::spread(epilepsy_marker_plot,cluster,avg_logFC)

library(ComplexHeatmap)
#mat<-as.numeric(as.matrix(heatmap_ep))
rownames(heatmap_ep)<-heatmap_ep$gene
heatmap_ep<-heatmap_ep[,-1]
#heatmap_ep[is.na(heatmap_ep)] <- 0
Heatmap(heatmap_ep,col = rev(rainbow(3)))



#disease gene enrich
gw23<-SubsetData(fc.all.combined,subset.name = "orig.ident",ident.remove = paste("GW",c("08","09","10","12","13","16","19","26"),sep = ""))
#subset(fc.all.combined,subset.name = "orig.ident",ident.use = paste("GW",c("08","09","10","12","13","16","19","26"),sep = ""))


gw23<-SubsetData(fc.s.combined,subset.name = "orig.ident",ident.remove = paste("GW",c("08","09","10","12","13","16","19","26"),sep = ""))

#
fc.s.combined<- AddMetaData(
object = fc.s.combined,
metadata = cct$ct,
col.name = 'celltype'
)
#fc.EX<-subset(fc.s.combined,cells = fc.ex.id)

fc.EX<-subset(fc.all.combined,cells = fc.ex.id)
DefaultAssay(fc.EX) <- "RNA"
fc.EX <- NormalizeData(fc.EX)
fc.EX <- FindVariableFeatures(fc.EX, selection.method = "vst", nfeatures = 2000)

tang.GW23.ex <- CreateSeuratObject(counts = as.matrix(fc.all.combined@assays$RNA[,ex.celllist.GW23]), project = "tang exneurons GW23")
tang.GW23.ex$batch <- "tang exneurons GW23"
tang.GW23.ex$batch2 <- "tang exneurons GW23"
tang.GW23.ex[["percent.mt"]] <- PercentageFeatureSet(object = tang.GW23.ex, pattern = "^MT-")
FeatureScatter(tang.GW23.ex, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
tang.GW23.ex <- subset(tang.GW23.ex,subset = nFeature_RNA > 500 & percent.mt < 5)
tang.GW23.ex <- NormalizeData(tang.GW23.ex)
tang.GW23.ex <- FindVariableFeatures(tang.GW23.ex, selection.method = "vst", nfeatures = 2000)

ex.anchors <- FindIntegrationAnchors(object.list = list(fc.EX, tang.GW23.ex), dims = 1:30,k.filter = 20)
ex.combined <- IntegrateData(anchorset = ex.anchors, dims = 1:30)
#DefaultAssay(ex.combined) <- "integrated"
ex.combined <- ScaleData(ex.combined)
ex.combined <- RunPCA(ex.combined,npcs = 30)
ElbowPlot(ex.combined,ndims = 30)

ex.combined <- RunUMAP(ex.combined, reduction = "pca", dims = 1:20)
ex.combined <- FindNeighbors(ex.combined, reduction = "pca", dims = 1:20)
ex.combined <- FindClusters(ex.combined, resolution = 0.8)

##find conserve markers
DefaultAssay(ex.combined) <- "RNA"
#fc.s.conserve.markers <- FindConservedMarkers(fc.s.combined, ident.1 = 15, grouping.var = "batch")
#fc.s.markers <- FindAllMarkers(fc.s.combined, only.pos = TRUE, min.pct = 0.25, logfc.s.threshold = 0.25)
ex.combined <- SetIdent(ex.combined,value = "batch2")
ex.GW23.markers <- FindMarkers(ex.combined,ident.1 = "T21_frontal", ident.2 = "tang exneurons GW23",only.pos = F, min.pct = 0.25, logfc.s.threshold = 0.25)
#wang_ex<-read.table("EXintegrate/exfc_singlet_markers.txt",header = T)
write.table(ex.GW23.markers,"exfc_singlet_markers.txt",quote = F,sep = "\t",row.names = T)

#APP, NCAM2, DSCAM, WRB, U2AF1 and SON 

up_ex<-ex.GW23.markers[which(ex.GW23.markers$avg_logFC>0),]
intersect(rownames(ex.GW23.markers2),c("APP", "NCAM2", "DSCAM", "WRB", "U2AF1","SON"))

ex.GW23.markers[c("APP", "NCAM2", "DSCAM", "WRB", "U2AF1","SON"),]

ex.GW23.markers <- read.table("/Users/wangshiyou/Documents/work/BGI/program/T21_whole_cortex/frontal/singlet_result/EXintegrate/exfc_singlet_markers.txt",header = T,sep = "\t")
gene.chr21 <- read.table("/Users/wangshiyou/Documents/work/BGI/program/21三体/T21brain/allgene21chr.txt",header = F)
TFs <- read.table("/Users/wangshiyou/Documents/work/BGI/program/reference/TFs_list.txt",header = F)

##volcano plot
#diffexp.genes$change <-  as.factor(ifelse(diffexp.genes$p_val_adj < 0.01 & abs(diffexp.genes$avg_logFC) > 2,ifelse(diffexp.genes$avg_logFC > 2,'UP','DOWN'),'NOT'))
ex.GW23.markers2 <- ex.GW23.markers[ex.GW23.markers$p_val_adj < 0.01,]
ex.GW23.markers2$change <-  as.factor(ifelse(ex.GW23.markers2$p_val_adj < 0.01 & abs(ex.GW23.markers2$avg_logFC) > 1,ifelse(ex.GW23.markers2$avg_logFC > 1,'UP','DOWN'),'NOT'))
ex.GW23.markers2$sign <- NA
gene.chr21.diff <- intersect(rownames(ex.GW23.markers2[c(which(ex.GW23.markers2$avg_logFC > 1),which(ex.GW23.markers2$avg_logFC < -1)),]),gene.chr21$Symbol)
#TFs<-read.table("./../../../human_TF.txt",sep = "\t",header = T)
TFs.diff <- intersect(TFs$Symbol,rownames(ex.GW23.markers2[c(which(ex.GW23.markers2$avg_logFC > 1),which(ex.GW23.markers2$avg_logFC < -1)),]))

ex.GW23.markers2[gene.chr21.diff,7] <- gene.chr21.diff
ex.GW23.markers2[TFs.diff,7] <- TFs.diff

#####

write.table(ex.GW23.markers2,"diff_genes(FC_1_P_value_0.01).txt",quote = F,sep = "\t")

library(ggplot2)
library(ggrepel)
ex.GW23.markers2$p_final<-(-log10(ex.GW23.markers2$p_val_adj+1))
p <- ggplot(data = ex.GW23.markers2, aes(x = avg_logFC, y = log10(p_val_adj+1), color = change)) +
  geom_point(alpha=0.8, size = 0.5) +
  theme_bw(base_size = 15) +
  theme(
    panel.grid.minor = element_blank(),
    panel.grid.major = element_blank()
  ) +ylab("-log10(P+1).adj")+
  scale_color_manual(name = "", values = c("#BC3C28","#0072B5", "grey"), limits = c("UP", "DOWN", "NOT")) +
  geom_text_repel(aes(label = sign)
                  , box.padding = unit(0.3, "lines")
                  ,point.padding = unit(1.4, "lines")
                  , show.legend = F, size = 3)
p

pdf("exneuron diff exp genes volcano GW23.pdf",height = 6,width = 7.5)
print(p)
dev.off()

ex.genes.heatmap.plot <- na.omit(ex.GW23.markers2$sign)
h <- DoHeatmap(ex.combined,features = ex.genes.heatmap.plot,group.by = "batch2",slot = "data")+
  scale_fill_gradient2(
    low = "#0000FF", 
    high = "#FF0000",
    mid = "#FFFFFF")
h
pdf("diff TFs and chr21 genes heatmap GW23.pdf",height = 6,width = 8.5)
print(h)
dev.off()

Tang<-read.table("./GSE104276_all_pfc_2394_UMI_count_NOERCC.xls")
#xls2csv(xls, sheet=1, verbose=FALSE, blank.lines.skip=TRUE, ..., perl="perl")
#fc.tang <- CreateSeuratObject(counts =Tang, project = "Tang_prefrontal", min.cells = as.integer(dim(fc3.data)[2]/1000))
#fc.tang <- CreateSeuratObject(counts =Tang, project = "Tang_prefrontal", min.cells = 7)
fc.tang <- CreateSeuratObject(counts =Tang, project = "Tang_prefrontal")

fc.tang$batch <- "Tang_prefrontal"
fc.tang$batch2 <- "Tang_prefrontal"
fc.tang[["percent.mt"]] <- PercentageFeatureSet(object = fc.tang, pattern = "^MT-")
FeatureScatter(fc.tang, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
fc.tang <- subset(fc.tang,subset = nFeature_RNA > 500 & percent.mt < 5)
fc.tang <- NormalizeData(fc.tang)
fc.tang <- FindVariableFeatures(fc.tang, selection.method = "vst", nfeatures = 2000)



####################################################################################
##tang fuchou data
####################################################################################
fc.all.anchors <- FindIntegrationAnchors(object.list = list(fc.s.combined,fc.tang), dims = 1:30)
fc.all.combined <- IntegrateData(anchorset = fc.all.anchors, dims = 1:30)

fc.all.combined <- ScaleData(fc.all.combined)
fc.all.combined <- RunPCA(fc.all.combined, npcs = 30)
ElbowPlot(fc.all.combined,ndims = 30)

fc.all.combined <- RunUMAP(fc.all.combined, reduction = "pca", dims = 1:30)
fc.all.combined <- FindNeighbors(fc.all.combined, reduction = "pca", dims = 1:30)
fc.all.combined <- FindClusters(fc.all.combined, resolution = 1)

save(fc.all.combined,file = "fc_wangsy_wangxq_combined.Rdata")

##
MG_list<-read.xls("GSE104276_readme_sample_barcode.xlsx",sheet = 12)
MG_list$ident

fc.mg.id <- c()
for (i in c(18)) {
  fc.mg.id <- c(fc.mg.id,which(fc.s.combined@active.ident==i))
}
fc.mg.id <- colnames(fc.s.combined@assays$integrated@data)[fc.mg.id]


fc.MG<-subset(fc.all.combined,cells = fc.mg.id)
DefaultAssay(fc.MG) <- "RNA"
fc.MG <- NormalizeData(fc.MG)
fc.MG <- FindVariableFeatures(fc.MG, selection.method = "vst", nfeatures = 2000)
mg.celllist.GW23<-MG_list[which(MG_list$week=="GW23"),1]
tang.GW23.mg <- CreateSeuratObject(counts = as.matrix(fc.all.combined@assays$RNA[,mg.celllist.GW23]), project = "tang mg GW23")
tang.GW23.mg$batch <- "tang mg GW23"
tang.GW23.mg$batch2 <- "tang mg GW23"
tang.GW23.mg[["percent.mt"]] <- PercentageFeatureSet(object = tang.GW23.mg, pattern = "^MT-")
FeatureScatter(tang.GW23.mg, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
#tang.GW23.mg <- subset(tang.GW23.mg,subset = nFeature_RNA > 500 & percent.mt < 5)
tang.GW23.mg <- NormalizeData(tang.GW23.mg)
tang.GW23.mg <- FindVariableFeatures(tang.GW23.mg, selection.method = "vst", nfeatures = 2000)

mg.anchors <- FindIntegrationAnchors(object.list = list(fc.MG, tang.GW23.mg), dims = 1:9)
ex.combined <- IntegrateData(anchorset = ex.anchors, dims = 1:30)
#DefaultAssay(ex.combined) <- "integrated"
ex.combined <- ScaleData(ex.combined)
ex.combined <- RunPCA(ex.combined,npcs = 30)
ElbowPlot(ex.combined,ndims = 30)

ex.combined <- RunUMAP(ex.combined, reduction = "pca", dims = 1:20)
ex.combined <- FindNeighbors(ex.combined, reduction = "pca", dims = 1:20)
ex.combined <- FindClusters(ex.combined, resolution = 0.8)







