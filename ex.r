pdf("fig3_a_tra.pdf",height = 8,width = 12)
plot_cells(fc.m3.cds, color_cells_by="celltype", alpha = 1,cell_size=0.8,label_cell_groups=FALSE,show_trajectory_graph = FALSE,group_label_size=2.5,label_groups_by_cluster=FALSE,
           label_leaves=FALSE,
           label_branch_points=FALSE)+
  scale_color_manual(breaks = "celltype", values=colorRampPalette(brewer.pal(9,"Set3"))(length(unique(colData(fc.m3.cds)$celltype))))+
  theme(legend.title = element_text(color = "blue", size = 10))
dev.off()
setwd("/Users/xiangningding/Desktop/BGI/project/T21-brain/T21_hg/frontal/trajectory191114/all_cell_tra/")
fc.m3.cds<-readRDS("fc.monocle.cds.rds")
setwd("./../")
dir.create("IN")
setwd("IN")
pdf("fig3_b_tra_IN.pdf",height = 8,width = 10)
plot_cells(IN_fc.m3.cds, color_cells_by="seurat_clusters", alpha = 1,cell_size=1,label_cell_groups=FALSE,show_trajectory_graph = FALSE,group_label_size=2.5,label_groups_by_cluster=FALSE,
           label_leaves=FALSE,
           label_branch_points=FALSE)+
  #scale_color_manual(breaks = "celltype", values=colorRampPalette(brewer.pal(9,"Set3"))(length(unique(colData(fc.m3.cds)$celltype))))+
  theme(legend.title = element_text(color = "blue", size = 10))
dev.off()
pdf("fig3_b_tra_NPC.pdf",height = 8,width = 10)
plot_cells(NPC_fc.m3.cds, color_cells_by="seurat_clusters", alpha = 1,cell_size=1,label_cell_groups=FALSE,show_trajectory_graph = FALSE,group_label_size=2.5,label_groups_by_cluster=FALSE,
           label_leaves=FALSE,
           label_branch_points=FALSE)+
  #scale_color_manual(breaks = "celltype", values=colorRampPalette(brewer.pal(9,"Set3"))(length(unique(colData(fc.m3.cds)$celltype))))+
  theme(legend.title = element_text(color = "blue", size = 10))
dev.off()
pdf("fig3_b_tra_EX.pdf",height = 8,width = 10)
plot_cells(EX_fc.m3.cds, color_cells_by="seurat_clusters", alpha = 1,cell_size=1,label_cell_groups=FALSE,show_trajectory_graph = FALSE,group_label_size=2.5,label_groups_by_cluster=FALSE,
           label_leaves=FALSE,
           label_branch_points=FALSE)+
  #scale_color_manual(breaks = "celltype", values=colorRampPalette(brewer.pal(9,"Set3"))(length(unique(colData(fc.m3.cds)$celltype))))+
  theme(legend.title = element_text(color = "blue", size = 10))
dev.off()
#step1
IN_fc.m3.cds <- choose_cells(fc.m3.cds)
dim(IN_fc.m3.cds)
#[1] 26686  2152
IN_pr_test_res <- graph_test(IN_fc.m3.cds, neighbor_graph="principal_graph", cores=2)
IN_pr_deg_ids <- row.names(subset(IN_pr_test_res, q_value < 0.01))

IN_pr_deg_ids <- row.names(subset(IN_pr_test_res, q_value < 0.000000001))
input_ra<-unique(intersect(TF.gene$Symbol,IN_pr_deg_ids))

#step2
TF.gene<-read.table("./../../../../human_TF.txt",sep = "\t",header = T)
input_ra<-unique(intersect(TF.gene$Symbol,rownames(fc.m3.cds)))
#gene module
gene_module_df <- find_gene_modules(IN_fc.m3.cds[IN_pr_deg_ids,], resolution=c(0.1,0.01,0.001))
agg_mat <- aggregate_gene_expression(IN_fc.m3.cds, gene_module_df)
module_dendro <- hclust(dist(agg_mat))
gene_module_df$module <- factor(gene_module_df$module, 
                                levels = row.names(agg_mat)[module_dendro$order])
pdf("IN_branch_gene_module.pdf")
p<-plot_cells(IN_fc.m3.cds,
              genes=gene_module_df,
              label_cell_groups=FALSE,
              show_trajectory_graph=FALSE)
print(p)
dev.off()
#step3
#IN_genes <- c("MEIS2","POU2F2","FOXK1","NIFA")
for (gene in input_ra) {
  IN_lineage_cds <- fc.m3.cds[rowData(fc.m3.cds)$gene_short_name %in% gene,
                              colData(fc.m3.cds)$celltype %in% c("IN")]
  pdf(paste("IN_",gene,"_tf_test.pdf",sep = ""),height = 3,width = 8)
  p<-plot_genes_in_pseudotime(IN_lineage_cds,
                              color_cells_by="pseudotime",
                              min_expr=0.5)
  print(p)
  dev.off() 
}
