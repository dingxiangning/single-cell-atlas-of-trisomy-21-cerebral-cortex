library(JASPAR2020)
library(TFBSTools)
library(GenomicRanges)
library(motifmatchr)
library(GenomicFeatures)
library(BSgenome.Hsapiens.UCSC.hg38)
library(TxDb.Hsapiens.UCSC.hg38.knownGene)
library(org.Hs.eg.db)

#step01
species <- "Homo sapiens"
collection <- "CORE"
opts <- list()
opts["species"] <- species
opts["collection"] <- collection
out  <- TFBSTools::getMatrixSet(JASPAR2020, opts)

if (!isTRUE(all.equal(TFBSTools::name(out), names(out)))) 
  names(out) <- paste(names(out), TFBSTools::name(out), 
                      sep = "_")
motif <- out

motif_name<-names(motif)
jaspar_tf<-sapply(strsplit(as.character(names(motif)),'_'), "[",2)
jaspar_tf<-sapply(strsplit(as.character(jaspar_tf),'\\('), "[",1)

#step02
txdb <- TxDb.Hsapiens.UCSC.hg38.knownGene
promoter_txdb<-promoters(genes(txdb), upstream = 2000, downstream = 500)

gene <- as.character(candidate$targetGene)
mygene<-select(org.Hs.eg.db,columns=c("SYMBOL","ENTREZID"),keytype="SYMBOL",keys=gene)
mygene<-na.omit(mygene)
mygene<-mygene$ENTREZID

#step03
file<-list.files(pattern = "*linklist.Rdata") 

for (f in file) {
  load(f)
  candidate<-linkList[which(linkList$regulatoryGene %in% jaspar_tf),]
  target_filter<-data.frame(tf="tf",target_filterd="ta")
  for (i in unique(candidate$regulatoryGene)) {
    j<-grep(i,motif_name)
    if(length(j)>0){
      motif_j<-motif[motif_name[j]]
      #MA0476.1_FOS
      #motif_j<-motif["MA0476.1_FOS"]
      mygene<-unique(candidate[which(candidate$regulatoryGene %in% i),2])
      mygene<-select(org.Hs.eg.db,columns=c("SYMBOL","ENTREZID"),keytype="SYMBOL",keys=as.character(mygene))
      mygene<-na.omit(mygene)
      mygene<-unique(mygene$ENTREZID)
      mygene<-intersect(promoter_txdb$gene_id,mygene)
      motif_ix <- matchMotifs(motif_j, promoter_txdb[mygene], genome = "hg38")
      mat<-motifMatches(motif_ix)
      mygene<-mygene[which(as.character(mat)=="TRUE")]
      mygene<-select(org.Hs.eg.db,columns=c("SYMBOL","ENTREZID"),keytype="ENTREZID",keys=as.character(mygene))
      mygene<-na.omit(mygene)
      target_filter<-rbind(target_filter,data.frame(tf=i,target_filterd=paste(mygene$SYMBOL,collapse = ",")))
    }
  }
  target_filter<-target_filter[-c(1),]
  write.table(target_filter,file = paste(f,"_motif.txt",sep = ""),quote = F,row.names = F,col.names = T)
}
